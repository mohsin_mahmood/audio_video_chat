//
//  ResetViewController.m
//  Novitime
//
//  Created by Mohsin Mahmood on 4/12/14.
//  Copyright (c) 2014 Ruslan. All rights reserved.
//

#import "ResetViewController.h"

@interface ResetViewController ()

@end

@implementation ResetViewController
@synthesize email;
@synthesize activityIndicator;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.email.enabled = YES;
        forgetPass.enabled = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.activityIndicator.hidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)forgetPass:(id)sender
{
    [QBUsers resetUserPasswordWithEmail:email.text delegate:self];
    [self.activityIndicator startAnimating];
    self.email.enabled = NO;
    forgetPass.enabled = NO;
    self.activityIndicator.hidden=NO;
}
#pragma mark -
#pragma mark QBActionStatusDelegate

// QuickBlox API queries delegate
- (void)completedWithResult:(Result *)result
{
    self.activityIndicator.hidden=YES;
    // Success result
    if(result.success){
        // Set QuickBlox Chat delegate
        //
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                        message:@"Email Sent"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
        
        [alert release];
        
        [self performSegueWithIdentifier:@"backtoLogin" sender:self];
        
    }else {
        NSString *errorMessage = [[result.errors description] stringByReplacingOccurrencesOfString:@"(" withString:@""];
        errorMessage = [errorMessage stringByReplacingOccurrencesOfString:@")" withString:@""];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Errors"
                                                        message:errorMessage
                                                       delegate:nil
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles: nil];
        [alert show];
        
        [alert release];
    }
    self.email.enabled = YES;
    forgetPass.enabled = YES;
    
}


#pragma mark TextField Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.text=@"";
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
}

@end
