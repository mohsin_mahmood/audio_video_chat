//
//  LoginViewController.m
//  Novitime
//
//  Created by Mohsin Mahmood on 4/11/14.
//  Copyright (c) 2014 Ruslan. All rights reserved.
//


#import "Quickblox/Quickblox.h"
#import "LoginViewController.h"
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
//#define kPasswordforUser @"abcd1234"
#define kNLLoginKey @"kNLLoginKey"
#define kNLPassKey @"kNLPAssKey"
#define kNLPhoneKey @"kNLPhoneKey"
#define kNLEmailKey @"kNLEmailKey"
@interface LoginViewController ()

@end

@implementation LoginViewController
@synthesize activityIndicator;
@synthesize loginText;
@synthesize passwordText;
@synthesize phoneNumberText;
@synthesize emailText;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [self setActivityIndicator:nil];
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    [QBAuth createSessionWithDelegate:self];
    self.loginText.enabled = NO;
}

-(void)viewDidAppear:(BOOL)animated
{
    activityIndicator.hidden=YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc
{
    [activityIndicator release];
    [super dealloc];
}


- (IBAction)login:(id)sender
{
    if(self.loginText.enabled)
    {
        [self.loginText resignFirstResponder];
    }
    self.view.userInteractionEnabled=NO;
    [QBUsers logInWithUserLogin:self.loginText.text password:self.passwordText.text delegate:self];
    activityIndicator.hidden=NO;
    [activityIndicator startAnimating];
}

- (IBAction)forgetPassword:(id)sender
{
    [self performSegueWithIdentifier:@"resetPass" sender:self];
}

#pragma mark -
#pragma mark QBActionStatusDelegate

// QuickBlox API queries delegate
- (void)completedWithResult:(Result *)result
{
    
    // QuickBlox session creation  result
    if([result isKindOfClass:[QBUUserLogInResult class]] || [result isKindOfClass:[QBUUserResult class]]){
        NSString * className = NSStringFromClass ([result class]);
        // Success result
        
        
        if(result.success==YES && [className isEqualToString:@"QBUUserResult"])
        {
            [QBUsers logInWithUserLogin:self.loginText.text password:self.passwordText.text delegate:self];
            
        }
        
        else if(result.success ==YES && [className isEqualToString:@"QBUUserLogInResult"])
        {
            // Set QuickBlox Chat delegate
            //
            [QBChat instance].delegate = self;
            
            QBUUser *user = [QBUUser user];
            user.ID = ((QBUUserLogInResult *)result).user.ID;
            user.password = self.passwordText.text;
            // Login to QuickBlox Chat
            AppDelegate * delegate = [UIApplication sharedApplication].delegate;
            delegate.loggedInUser = self.loginText.text;
            delegate.user = user;
            [[QBChat instance] loginWithUser:user];
            NSString * userKey = [[NSUserDefaults standardUserDefaults] objectForKey:kNLLoginKey];
            if(!userKey)
            {
                [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithString:self.loginText.text]  forKey:kNLLoginKey];
                [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithString:self.passwordText.text]  forKey:kNLPassKey];
                [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithString:self.phoneNumberText.text]  forKey:kNLPhoneKey];
                [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithString:self.emailText.text]  forKey:kNLEmailKey];
            }
        }
        else if(!result.success && [className isEqualToString:@"QBUUserLogInResult"])
        {
            
            QBUUser *user = [QBUUser user];
            user.login = self.loginText.text;
            user.password = self.passwordText.text;
            user.email = self.emailText.text;
            user.phone = self.phoneNumberText.text;
            // create User
            [QBUsers signUp:user delegate:self];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops!"
                                                            message:[result.errors description]
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
            [alert release];
            [activityIndicator stopAnimating];
            activityIndicator.hidden=YES;
            self.view.userInteractionEnabled=YES;
        }
    }
    
    else if ([result isKindOfClass:[QBAAuthSessionCreationResult class]])
    {
        NSString * className = NSStringFromClass ([result class]);
        
        if([className isEqualToString:@"QBAAuthSessionCreationResult"])
        {
            NSString * userKey = [[NSUserDefaults standardUserDefaults] objectForKey:kNLLoginKey];
            NSString * pass = [[NSUserDefaults standardUserDefaults] objectForKey:kNLPassKey];
            NSString * phone = [[NSUserDefaults standardUserDefaults] objectForKey:kNLPhoneKey];
            NSString * Email = [[NSUserDefaults standardUserDefaults] objectForKey:kNLEmailKey];
            
            if(userKey)
            {
                if(userKey.length>1)
                {
                    self.loginText.text = userKey;
                    self.passwordText.text = pass;
                    self.emailText.text = Email;
                    self.phoneNumberText.text = phone;
                    
                    [self performSelectorOnMainThread:@selector(login:) withObject:nil waitUntilDone:NO];
                }
            }
            else
            {
                self.loginText.enabled = YES;
            }
        }
        
    }
}


#pragma mark -
#pragma mark QBChatDelegate



- (void)chatDidLogin{
    // Show Main controller
    [activityIndicator stopAnimating];
    activityIndicator.hidden=YES;
    self.view.userInteractionEnabled=YES;
    [self performSegueWithIdentifier:@"login" sender:self];
}

- (void)chatDidNotLogin{
    self.view.userInteractionEnabled=YES;
    [activityIndicator stopAnimating];
    activityIndicator.hidden=YES;
}

#pragma mark TextField Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.text=@"";
    if(textField == self.emailText || textField == self.passwordText)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480 || iOSDeviceScreenSize.height == 568 )
        {
            self.view.center = CGPointMake(self.view.center.x, iOSDeviceScreenSize.height/2 -60);
        }
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    if(textField == self.emailText || textField == self.passwordText)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480 || iOSDeviceScreenSize.height == 568 )
        {
            self.view.center = CGPointMake(self.view.center.x, iOSDeviceScreenSize.height/2);
        }
    }
    
}




@end
