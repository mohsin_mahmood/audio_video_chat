//
//  LoginViewController.h
//  Novitime
//
//  Created by Mohsin Mahmood on 4/11/14.
//  Copyright (c) 2014 Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UIAlertViewDelegate,QBActionStatusDelegate, QBChatDelegate,UITextFieldDelegate>{
    IBOutlet UIButton *loginButton;
    IBOutlet UIButton *resetButton;
    
}

@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (retain, nonatomic) IBOutlet UITextField * loginText;
@property (retain, nonatomic) IBOutlet UITextField * phoneNumberText;
@property (retain, nonatomic) IBOutlet UITextField * passwordText;
@property (retain, nonatomic) IBOutlet UITextField * emailText;

- (IBAction)login:(id)sender;
- (IBAction)forgetPassword:(id)sender;

@end
