//
//  ResetViewController.h
//  Novitime
//
//  Created by Mohsin Mahmood on 4/12/14.
//  Copyright (c) 2014 Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResetViewController : UIViewController
<QBActionStatusDelegate,UITextFieldDelegate>{
    
    IBOutlet UIButton *forgetPass;
    
}

@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (retain, nonatomic) IBOutlet UITextField * email;


- (IBAction)forgetPass:(id)sender;
@end
