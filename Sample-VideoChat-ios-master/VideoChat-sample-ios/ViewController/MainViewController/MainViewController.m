//
//  MainViewController.m
//  SimpleSample-videochat-ios
//
//  Created by QuickBlox team on 1/02/13.
//  Copyright (c) 2013 QuickBlox. All rights reserved.
//

#import "MainViewController.h"
#import "AppDelegate.h"

@interface MainViewController ()
@end
@implementation MainViewController
@synthesize user;
@synthesize videoChat;
@synthesize callAlert;
@synthesize users;
@synthesize userTable;
@synthesize userContainerView;

- (void)dealloc{
    [user release];
    self.users = nil;
    [super dealloc];
    
}

- (void)viewDidLoad
{
    self.users  = [[[NSMutableArray alloc] init] autorelease];
    [super viewDidLoad];
    
    AppDelegate * delegate = [UIApplication sharedApplication].delegate;
    self.user = delegate.user;
    
    opponentVideoView.layer.borderWidth = 1;
    opponentVideoView.layer.borderColor = [[UIColor grayColor] CGColor];
    opponentVideoView.layer.cornerRadius = 5;
    appIcon.hidden=YES;
    navBar.topItem.title = @"Online Users";
    self.userContainerView.backgroundColor = [UIColor blackColor];
}

- (void)viewDidUnload{
    
    callEndButton = nil;
    ringigngLabel = nil;
    myVideoView = nil;
    opponentVideoView = nil;
    navBar = nil;
    self.user=nil;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    // Start sending chat presence
    //
    [QBChat instance].delegate = self;
    timer = [NSTimer scheduledTimerWithTimeInterval:30 target:[QBChat instance] selector:@selector(sendPresence) userInfo:nil repeats:YES];
    
    //Setting table view size programatically
    self.userContainerView.backgroundColor = [UIColor blackColor];
    self.userTable = [[[UITableView alloc] initWithFrame:self.userContainerView.bounds style:UITableViewStylePlain] autorelease];
    self.userTable.delegate = self;
    self.userTable.dataSource = self;
    self.userTable.backgroundColor= [UIColor blackColor];
    
    [self.userContainerView addSubview:self.userTable];
    [self getAllContactsOfPhone];
    [self retrieveUsers:1];
    navBar.topItem.title = @"Online Users";
    
    
    
}

- (IBAction)videoOutputDidChange:(UIButton *)sender{
    if(self.videoChat != nil){
        self.videoChat.useBackCamera = !self.videoChat.useBackCamera;
    }
}

-(IBAction)mute:(id)sender
{
    UIButton * btn = sender;
    
    if(!btn.selected)
    {
        if(self.videoChat != nil){
            self.videoChat.microphoneEnabled = NO;
        }
        btn.selected = YES;
    }
    else
    {
        if(self.videoChat != nil){
            self.videoChat.microphoneEnabled = YES;
        }
        btn.selected = NO;
    }
    
}

- (void)backDone:(id)sender
{
    navBar.topItem.title = @"Online Users";
    // Finish call
    //
    [self.videoChat finishCall];
    myVideoView.hidden = YES;
    opponentVideoView.layer.borderWidth = 1;
    // release video chat
    //
    [[QBChat instance] unregisterVideoChatInstance:self.videoChat];
    self.videoChat = nil;
    [[QBChat instance] loginWithUser:self.user];
    self.userContainerView.hidden=NO;
    [self.userTable reloadData];
    navBar.topItem.title = @"Select User";
    ringigngLabel.hidden = NO;
}

- (IBAction)call:(id)sender{
    // Call
    // Setup video chat
    //
    if(self.videoChat == nil){
        self.videoChat = [[QBChat instance] createAndRegisterVideoChatInstance];
        self.videoChat.viewToRenderOpponentVideoStream = opponentVideoView;
        self.videoChat.viewToRenderOwnVideoStream = myVideoView;
    }
    
    // Set Audio & Video output
    //
    self.videoChat.useHeadphone = 0;
    self.videoChat.useBackCamera = 0;
    
    // Call user by ID
    //
    [self UpdateSettingsOfCallQuality];
    [self.videoChat callUser:self.user.ID conferenceType:QBVideoChatConferenceTypeAudioAndVideo];
    
    ringigngLabel.hidden = NO;
    ringigngLabel.text = @"Calling...";
}

-(IBAction)endCall:(id)sender
{
    appIcon.hidden=YES;
    [self backDone:sender];
    [self.users removeAllObjects];
    self.users = [[NSMutableArray alloc] init];
    [self.userTable reloadData];
    [self retrieveUsers:1];
}

- (void)reject{
    // Reject call
    //
    if(self.videoChat == nil){
        self.videoChat = [[QBChat instance] createAndRegisterVideoChatInstanceWithSessionID:sessionID];
    }
    [self.videoChat rejectCallWithOpponentID:videoChatOpponentID];
    //
    //
    [[QBChat instance] unregisterVideoChatInstance:self.videoChat];
    self.videoChat = nil;
    [[QBChat instance] loginWithUser:self.user];
    
    // update UI
    ringigngLabel.hidden = YES;
    
    // release player
    [ringingPlayer release];
    ringingPlayer = nil;
}

- (void)accept{
    NSLog(@"accept");
    self.userContainerView.hidden=YES;
    
    // Setup video chat
    //
    if(self.videoChat == nil){
        self.videoChat = [[QBChat instance] createAndRegisterVideoChatInstanceWithSessionID:sessionID];
        self.videoChat.viewToRenderOpponentVideoStream = opponentVideoView;
        self.videoChat.viewToRenderOwnVideoStream = myVideoView;
    }
    
    // Set Audio & Video output
    //
    self.videoChat.useHeadphone = 0;
    self.videoChat.useBackCamera = 0;
    
    // Accept call
    //
    [self UpdateSettingsOfCallQuality];
    [self.videoChat acceptCallWithOpponentID:videoChatOpponentID conferenceType:videoChatConferenceType];
    

    ringigngLabel.hidden = YES;
    opponentVideoView.layer.borderWidth = 0;
    myVideoView.hidden = NO;
    appIcon.hidden=NO;
    
    [ringingPlayer release];
    ringingPlayer = nil;
}

- (void)hideCallAlert{
    [self.callAlert dismissWithClickedButtonIndex:-1 animated:YES];
    self.callAlert = nil;
}

#pragma mark -
#pragma mark AVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    [ringingPlayer release];
    ringingPlayer = nil;
}


#pragma mark -
#pragma mark QBChatDelegate
//
// VideoChat delegate

-(void) chatDidReceiveCallRequestFromUser:(NSUInteger)userID withSessionID:(NSString *)_sessionID conferenceType:(enum QBVideoChatConferenceType)conferenceType{
    NSLog(@"chatDidReceiveCallRequestFromUser %d", userID);
    
    // save  opponent data
    videoChatOpponentID = userID;
    videoChatConferenceType = conferenceType;
    [sessionID release];
    sessionID = [_sessionID retain];
    
    // show call alert
    //
    if (self.callAlert == nil) {
        NSString *message = [NSString stringWithFormat:@"%@ is calling. Would you like to answer?",self.user.login];
        self.callAlert = [[UIAlertView alloc] initWithTitle:@"Call" message:message delegate:self cancelButtonTitle:@"Decline" otherButtonTitles:@"Accept", nil];
        [self.callAlert show];
    }
    
    // hide call alert if opponent has canceled call
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideCallAlert) object:nil];
    [self performSelector:@selector(hideCallAlert) withObject:nil afterDelay:4];
    
    // play call music
    //
    if(ringingPlayer == nil){
        NSString *path =[[NSBundle mainBundle] pathForResource:@"Ringing" ofType:@"mp3"];
        NSURL *url = [NSURL fileURLWithPath:path];
        ringingPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:NULL];
        ringingPlayer.delegate = self;
        [ringingPlayer setVolume:1.0];
        [ringingPlayer play];
    }
}

-(void) chatCallUserDidNotAnswer:(NSUInteger)userID{
    NSLog(@"chatCallUserDidNotAnswer %d", userID);
    ringigngLabel.hidden = YES;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Novilight" message:@"User isn't answering. Please try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
    [alert release];
}

-(void) chatCallDidRejectByUser:(NSUInteger)userID{
    NSLog(@"chatCallDidRejectByUser %d", userID);
    
    ringigngLabel.hidden = YES;
    appIcon.hidden=YES;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Novilight" message:@"User has rejected your call." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
    [alert release];
}

-(void) chatCallDidAcceptByUser:(NSUInteger)userID{
    NSLog(@"chatCallDidAcceptByUser %d", userID);
    
    ringigngLabel.hidden = YES;
    opponentVideoView.layer.borderWidth = 0;
    myVideoView.hidden = NO;
    opponentVideoView.hidden=NO;
    appIcon.hidden=NO;
}

-(void) chatCallDidStopByUser:(NSUInteger)userID status:(NSString *)status{
    NSLog(@"chatCallDidStopByUser %@ purpose %@", user.login, status);
    
    if([status isEqualToString:kStopVideoChatCallStatus_OpponentDidNotAnswer]){
        
        self.callAlert.delegate = nil;
        [self.callAlert dismissWithClickedButtonIndex:0 animated:YES];
        self.callAlert = nil;
        
        ringigngLabel.hidden = YES;
        
        [ringingPlayer release];
        ringingPlayer = nil;
        
    }else{
        myVideoView.hidden = YES;
        opponentVideoView.layer.borderWidth = 1;
        opponentVideoView.image = nil;
    }
    
    appIcon.hidden=YES;
    
    // release video chat
    //
    [[QBChat instance] unregisterVideoChatInstance:self.videoChat];
    self.videoChat = nil;
    [[QBChat instance] loginWithUser:self.user];
}

- (void)chatCallDidStartWithUser:(NSUInteger)userID sessionID:(NSString *)sessionID{
}

- (void)didStartUseTURNForVideoChat{
    //    NSLog(@"_____TURN_____TURN_____");
}


#pragma mark -
#pragma mark UIAlertView

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
            // Reject
        case 0:
            [self reject];
            break;
            // Accept
        case 1:
            [self accept];
            break;
            
        default:
            break;
    }
    
    self.callAlert = nil;
}


#pragma mark code for fetching users

#pragma mark -
#pragma mark QBActionStatusDelegate

- (void) retrieveUsers:(int)page{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    
    // retrieve 100 users
    PagedRequest* request = [[PagedRequest alloc] init];
    request.perPage = 100;
    request.page=page;
	[QBUsers usersWithPagedRequest:request delegate:self];
	[request release];
}

// QuickBlox API queries delegate
- (void)completedWithResult:(Result *)result
{
    // Get User result
    if(result.success && [result isKindOfClass:[QBUUserResult class]])
    {
        
    }
    
    // Retrieve Users result
    else if([result isKindOfClass:[QBUUserPagedResult class]])
    {
        // Success result
        if (result.success)
        {
            // update table
            QBUUserPagedResult *usersSearchRes = (QBUUserPagedResult *)result;
            NSArray * users01 =  usersSearchRes.users;
            lastRetrievelTime = [[NSDate date] timeIntervalSince1970];
            for(QBUUser * user01 in users01)
            {
                if(user01.ID == self.user.ID)
                {
                    continue;
                }
                if([self containsInPhoneBook:user01.phone])
                {
                    [self.users addObject:user01];
                }
            }
            [self.userTable reloadData];
            
            if(usersSearchRes.currentPage < usersSearchRes.totalPages)
            {
                [self retrieveUsers:(int)usersSearchRes.currentPage + 1];
            }
            // Errors
        }else{
            NSLog(@"Errors=%@", result.errors);
        }
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
}

-(BOOL)containsInPhoneBook:(NSString *)number
{
    BOOL contains = NO;
    if(number)
    {
        NSString * numtemp = number;
        if(number.length>9)
        {
            numtemp = [number substringFromIndex:3];
        }
        
        for(NSString* phone in phoneBookContacts)
        {
            if(!([phone rangeOfString:numtemp].location == NSNotFound))
            {
                contains = YES;
            }
        }
    }
    return contains;
}

#pragma mark -
#pragma mark TableViewDataSource & TableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // show user details
    //more data on calling
    AppDelegate * delegate = [UIApplication sharedApplication].delegate;
    delegate.user = [self.users objectAtIndex:indexPath.row];
    self.user = delegate.user;
    navBar.topItem.title = delegate.loggedInUser;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.userContainerView.hidden=YES;
    selectedUSer = [self.users objectAtIndex:indexPath.row];
    [self call:nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.users count];
}

// Making table view using custom cells
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* SimpleTableIdentifier = @"SimpleTableIdentifier";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:SimpleTableIdentifier];
    if (cell == nil)
    {
        
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:SimpleTableIdentifier] autorelease];
    }
    QBUUser* obtainedUser = [self.users objectAtIndex:[indexPath row]];
    cell.backgroundColor = [UIColor blackColor];
    
    if(obtainedUser.login != nil){
        cell.textLabel.text = obtainedUser.login;
        cell.detailTextLabel.text = obtainedUser.phone;
        cell.textLabel.textColor = [UIColor colorWithRed:32 green:138 blue:143 alpha:1];
        cell.detailTextLabel.textColor = [UIColor colorWithRed:32 green:138 blue:143 alpha:1];
        if([self checkIsOnline:obtainedUser lastTime:lastRetrievelTime])
        {
            cell.textLabel.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
        }
        else
        {
            cell.textLabel.font = [UIFont italicSystemFontOfSize:[UIFont systemFontSize]];
        }
        
    }
    
    return cell;
}

-(BOOL) checkIsOnline:(QBUUser*) UserStat lastTime:(NSInteger)currentTime
{
    
    NSInteger userLastRequestAtTimeInterval   = [UserStat.lastRequestAt timeIntervalSince1970];
    
    // if user didn't do anything last 5 minutes (5*60 seconds)
    if((currentTime - userLastRequestAtTimeInterval) > 5*60){
        return NO;
    }
    return YES;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}


-(void)getAllContactsOfPhone
{
    if(phoneBookContacts)
    {
        [phoneBookContacts removeAllObjects];
        [phoneBookContacts release];
    }
    phoneBookContacts  = [[NSMutableArray alloc] init];
    
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
            
        });
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        
        CFErrorRef *error = NULL;
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
        CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
        CFIndex numberOfPeople = ABAddressBookGetPersonCount(addressBook);
        
        for(int i = 0; i < numberOfPeople; i++) {
            
            ABRecordRef person = CFArrayGetValueAtIndex( allPeople, i );
            
            ABMultiValueRef phoneNumbers = ABRecordCopyValue(person, kABPersonPhoneProperty);
            [[UIDevice currentDevice] name];
            
            //NSLog(@"\n%@\n", [[UIDevice currentDevice] name]);
            
            for (CFIndex i = 0; i < ABMultiValueGetCount(phoneNumbers); i++) {
                NSString *phoneNumber = (__bridge NSString *) ABMultiValueCopyValueAtIndex(phoneNumbers, i);
                phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
                phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
                phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
                
                [phoneBookContacts addObject:phoneNumber];
            }
        }
        NSLog(@"AllNumber:%@",[phoneBookContacts description]);
    }
    else {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"Please allow from settings to access Contacts" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
}

-(void)UpdateSettingsOfCallQuality
{
    NSMutableDictionary *videoChatConfiguration = [[QBSettings videoChatConfiguration] mutableCopy];
    [videoChatConfiguration setObject:AVCaptureSessionPresetMedium forKey:kQBVideoChatFrameQualityPreset];
    [videoChatConfiguration setObject:@30 forKey:kQBVideoChatVideoFramesPerSecond];
    [QBSettings setVideoChatConfiguration:videoChatConfiguration];
    [videoChatConfiguration release];
}

@end
