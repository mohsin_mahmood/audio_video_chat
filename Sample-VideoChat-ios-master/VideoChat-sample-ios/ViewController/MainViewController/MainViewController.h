//
//  MainViewController.h
//  SimpleSample-videochat-ios
//
//  Created by QuickBlox team on 1/02/13.
//  Copyright (c) 2013 QuickBlox. All rights reserved.
//
//
// This class demonstrates how to work with VideoChat API.
// It shows how to setup video conference between 2 users
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AVFoundation/AVFoundation.h>

@interface MainViewController : UIViewController <QBActionStatusDelegate,QBChatDelegate, AVAudioPlayerDelegate, UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate>{
    IBOutlet UIButton *callEndButton;
    IBOutlet UILabel *ringigngLabel;
    IBOutlet UIImageView *opponentVideoView;
    IBOutlet UIImageView *myVideoView;
    IBOutlet UINavigationBar *navBar;
    IBOutlet UIButton *videoOutput;
    IBOutlet UILabel * appIcon;
    long lastRetrievelTime;
    
    NSTimer * timer;
    AVAudioPlayer *ringingPlayer;
    
    //
    NSUInteger videoChatOpponentID;
    enum QBVideoChatConferenceType videoChatConferenceType;
    NSString *sessionID;
    NSMutableArray * phoneBookContacts;
    QBUUser * selectedUSer;
}

@property (retain) QBVideoChat *videoChat;
@property (retain) UIAlertView *callAlert;
@property (nonatomic, retain) QBUUser* user;
@property (nonatomic, retain) NSMutableArray * users;
@property (nonatomic, retain) UITableView * userTable;
@property (nonatomic, retain) IBOutlet UIView * userContainerView;
- (IBAction)call:(id)sender;
-(IBAction)endCall:(id)sender;
-(IBAction)mute:(id)sender;
- (void)reject;
- (void)accept;

@end
